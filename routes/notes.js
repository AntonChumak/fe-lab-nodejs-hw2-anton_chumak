const express = require('express');
const router = new express.Router();
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcryptjs');
// const config = require('config');
const auth = require('../middleware/auth');

const User = require('../models/User');
const Note = require('../models/Note');

/**
* @route    GET api/notes
* @desc     Get notes for User
* @access   Public
**/
router.get('/', auth, async (req, res)=>{
  const offset = req.query.offset;
  const limit = req.query.limit;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const notes = await Note.find({userId: user['_id']})
        .skip(offset).limit(limit).select('-__v');
    res.json({'offset': offset,
      'limit': limit,
      'count': notes.length,
      'notes': notes});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});
/**
 * @route    POST api/notes
 * @desc     Add Note for User
 * @access   Public
**/
router.post('/', auth, async (req, res)=>{
  const text = req.body.text;
  const user = await User.findById(req.user.id).select('-password -__v');
  // console.log(user.id, text);
  try {
    const note = new Note({
      userId: user.id,
      text: text});
    await note.save();
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});
/**
 * @route    GET api/notes/id
 * @desc     Get a Note for User
 * @access   Public
 **/
router.get('/:id', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const notes = await Note.find({userId: user['_id'], _id: id})
        .select('-__v');
    res.json({'note': notes[0]});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});
/**
 * @route    GET api/notes/id
 * @desc     Get a Note for User
 * @access   Public
 **/
router.put('/:id', auth, async (req, res)=>{
  const text = req.body.text;
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const notes = await Note.find({userId: user['_id'], _id: id})
        .select('-__v');
    notes[0].text = await text;
    await notes[0].save();
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});
/**
 * @route    PATCH api/notes/id
 * @desc     Get a Note for User
 * @access   Public
 **/
router.patch('/:id', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const notes = await Note.find({userId: user['_id'], _id: id})
        .select('-__v');
    notes[0].completed = !notes[0].completed;
    await notes[0].save();
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});
/**
 * @route    DELETE api/notes/id
 * @desc     Get a Note for User
 * @access   Public
 **/
router.delete('/:id', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    await Note.deleteOne({userId: user['_id'], _id: id});
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
